# BRD public

This repository is intended to provide access to the models of Bovine Respiratory Disease (BRD) developed in the Dynamo team, BIOEPAR, and associated with publications.

BRD models are available by tags or branches

## AVAILABLE MODELS

### Single-batch, average pathogen model

- branch: `avg-pathogen-single-batch`
- associated publication (tag "`Veterinary-Reseach-2022`"): 

    S. Picault, P. Ezanno, K. Smith, D. Amrine, B. White, S. Assié, **2022**.<br>
    Modelling the effects of antimicrobial metaphylaxis and pen size on bovine respiratory disease in high and low risk fattening cattle.<br>
    *Veterinary Research* **53** (77).<br> 
    [https://doi.org/10.1186/s13567-022-01094-1](https://doi.org/10.1186/s13567-022-01094-1)

## LICENSE

Unless specified, the models are distributed under the [CC-BY-NC-SA 4.0 LICENSE](https://creativecommons.org/licenses/by-nc-sa/4.0/)
